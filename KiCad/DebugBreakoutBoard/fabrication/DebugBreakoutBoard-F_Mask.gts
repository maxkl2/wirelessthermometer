%TF.GenerationSoftware,KiCad,Pcbnew,7.0.6*%
%TF.CreationDate,2023-08-05T16:47:49+02:00*%
%TF.ProjectId,DebugBreakoutBoard,44656275-6742-4726-9561-6b6f7574426f,rev?*%
%TF.SameCoordinates,Original*%
%TF.FileFunction,Soldermask,Top*%
%TF.FilePolarity,Negative*%
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW 7.0.6) date 2023-08-05 16:47:49*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
G04 Aperture macros list*
%AMRoundRect*
0 Rectangle with rounded corners*
0 $1 Rounding radius*
0 $2 $3 $4 $5 $6 $7 $8 $9 X,Y pos of 4 corners*
0 Add a 4 corners polygon primitive as box body*
4,1,4,$2,$3,$4,$5,$6,$7,$8,$9,$2,$3,0*
0 Add four circle primitives for the rounded corners*
1,1,$1+$1,$2,$3*
1,1,$1+$1,$4,$5*
1,1,$1+$1,$6,$7*
1,1,$1+$1,$8,$9*
0 Add four rect primitives between the rounded corners*
20,1,$1+$1,$2,$3,$4,$5,0*
20,1,$1+$1,$4,$5,$6,$7,0*
20,1,$1+$1,$6,$7,$8,$9,0*
20,1,$1+$1,$8,$9,$2,$3,0*%
G04 Aperture macros list end*
%ADD10RoundRect,0.250000X-0.600000X-0.600000X0.600000X-0.600000X0.600000X0.600000X-0.600000X0.600000X0*%
%ADD11C,1.700000*%
%ADD12O,1.700000X1.700000*%
%ADD13R,1.700000X1.700000*%
G04 APERTURE END LIST*
D10*
%TO.C,J2*%
X55880000Y-33020000D03*
D11*
X58420000Y-33020000D03*
X55880000Y-35560000D03*
X58420000Y-35560000D03*
X55880000Y-38100000D03*
X58420000Y-38100000D03*
%TD*%
D12*
%TO.C,J1*%
X33020000Y-38100000D03*
X30480000Y-38100000D03*
X33020000Y-35560000D03*
X30480000Y-35560000D03*
X33020000Y-33020000D03*
D13*
X30480000Y-33020000D03*
%TD*%
M02*
