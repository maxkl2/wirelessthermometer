While https://gitlab.com/maxkl2/precisetemp has proven very useful, it also has some limitations. The biggest being that, while a data log is running, it is difficult to see the current temperature without disturbing the measurement. Additionally, the humidity sensor has a dirt- and liquid-sensitive opening which limits the applications.

This further development aims to solve that by replacing the LCD with a Zigbee interface and the separate temperature and humidity sensors with a combined one with PTFE membrane. It also adds a bigger battery.

![PCB front render](KiCad/WirelessThermometer/renders/front-cropped.png)

