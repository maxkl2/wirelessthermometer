
Uses any TI eZ-FET or MSP-FET with EnergyTrace hardware to measure power consumption.

# Usage

To measure for 60 seconds and store the samples in energytrace.txt:

```sh
./energytrace 60 > energytrace.txt
```

# Building

The MSPDebugStack needs to be installed first, otherwise libmsp430 and corresponding header files won't be found.

Then build with

```sh
gcc -o energytrace src/main.c -lmsp430 -Wall -Wextra -Werror
```
