
import argparse

from matplotlib import pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np


battery_capacity_CR123A = 4.5 # Wh


parser = argparse.ArgumentParser(description='Plot EnergyTrace measurements.')
parser.add_argument('filename',
                    help='file containing the measurement data')
parser.add_argument('-c', '--capacity', type=float, default=battery_capacity_CR123A,
                    help=f'battery capacity in Wh (default: {battery_capacity_CR123A})')
parser.add_argument('--plot-current', action='store_true',
                    help='also plot the current data (which EnergyTrace probably reports incorrectly)')
args = parser.parse_args()


data = np.genfromtxt(args.filename, invalid_raise=False)

data_t = data[:,0] * 1e-6 # us to s
data_current = data[:,1] * 1e-9 # nA to A
data_voltage = data[:,2] * 1e-3 # mV to V
data_energy = data[:,3] * 100e-9 # uWs to J


# Voltage data (probably ok)
mean_voltage = np.mean(data_voltage)


# Current data (appears to be buggy)
mean_current = np.mean(data_current)
median_current = np.median(data_current)


# Energy data (probably ok)
time_diff = np.diff(data_t, prepend=0)
energy_diff = np.diff(data_energy, prepend=0)
power_from_energy = energy_diff / time_diff
current_from_energy = power_from_energy / data_voltage

total_time = data_t[-1] - data_t[0]
total_energy = data_energy[-1] - data_energy[0]
mean_power = total_energy / total_time
mean_power_from_dedt = np.mean(power_from_energy)
# median_power = np.median(power_from_energy)
mean_current_from_energy = mean_power / mean_voltage

battery_capacity = args.capacity
battery_life_hours = battery_capacity / mean_power
battery_life_days = battery_life_hours / 24
battery_life_months = battery_life_days / 30
battery_life_years = battery_life_days / 365

# Heuristic to approximate idle power by excluding peaks
threshold_idle = np.median(power_from_energy[power_from_energy > 1e-9]) * 2
indices_idle = np.argwhere(power_from_energy < threshold_idle)
t_idle = data_t[indices_idle]
power_idle = power_from_energy[indices_idle]
mean_power_idle = np.mean(power_idle)
mean_current_idle = mean_power_idle / mean_voltage


eng_fmt = lambda v, u, p=3: EngFormatter(unit=u, places=p)(v)

print(f'Mean voltage: {eng_fmt(mean_voltage, 'V')}')
print()
print('From current data (potentially incorrect):')
print(f' Mean current: {eng_fmt(mean_current, 'A')}')
print(f' Median current: {eng_fmt(median_current, 'A')}')
print()
print('From energy data:')
print(f' Total energy: {eng_fmt(total_energy, 'J')}')
print(f' Mean power: {eng_fmt(mean_power, 'W')} (from total energy), {eng_fmt(mean_power_from_dedt, 'W')} (via dE/dt)')
print(f' Mean idle power: {eng_fmt(mean_power_idle, 'W')} ≙ {eng_fmt(mean_current_idle, 'A')}')
print(f' Mean current: {eng_fmt(mean_current_from_energy, 'A')}')
print(f' Battery life: {eng_fmt(battery_capacity, 'Wh')} / {eng_fmt(mean_power, 'W')} = {battery_life_hours:.2f} h = {battery_life_months:.2f} mon = {battery_life_years:.2f} yr')
# print(f'Median power: {eng_fmt(median_power, 'W')}')


fig = plt.figure(figsize=(8, 6), dpi=100)
ax1 = fig.add_subplot()
# ax1 = fig.add_subplot(1, 2, 1)

ax1.plot(data_t, power_from_energy, label='p_dcdc')
ax1.axhline(mean_power, color='k', linestyle='--', label=f'mean: {eng_fmt(mean_power, 'W')}')
ax1.axhline(mean_power_idle, color='g', linestyle='--', label=f'idle: {eng_fmt(mean_power_idle, 'W')}')
ax1.xaxis.set_major_formatter(EngFormatter(unit='s', places=2))
ax1.yaxis.set_major_formatter(EngFormatter(unit='W', places=2))

plot_current = args.plot_current

if plot_current:
    ax2 = ax1.twinx()
    ax2.plot(data_t, data_current, 'r', label='i_dcdc')
    ax2.yaxis.set_major_formatter(EngFormatter(unit='A', places=2))
    p_to_i = lambda p: p / mean_voltage
    plims = ax1.get_ylim()
    ax2.set_ylim(p_to_i(plims[0]), p_to_i(plims[1]))

if plot_current:
    lines, labels = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(lines + lines2, labels + labels2, loc=0)
else:
    ax1.legend()

# ax_hist = fig.add_subplot(1, 2, 2, sharey=ax1)
# ax_hist.hist(data_current, bins=50, orientation='horizontal')
# ax_hist.tick_params(axis='y', labelleft=False)
# ax_hist.semilogx()

fig.tight_layout()

plt.show()
