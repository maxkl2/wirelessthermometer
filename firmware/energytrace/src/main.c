
#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdlib.h>

#include <MSP430.h>
#include <MSP430_EnergyTrace.h>
#include <MSP430_Debug.h>

#define DEFAULT_VCC 3300
#define DEFAULT_DURATION 10

struct __attribute__((packed)) event_header {
    uint8_t id;
    uint64_t timestamp:56; // us
};

struct __attribute__((packed)) event_data_8 {
    uint32_t current; // nA
    uint16_t voltage; // mV
    uint32_t energy; // uWs = 100 nJ
};

static volatile unsigned int cb_counter, sample_counter;

static void et_callback_data(void *pContext, const uint8_t *pBuffer, uint32_t nBufferSize) {
    (void) pContext;

    cb_counter++;

    while (nBufferSize >= sizeof(struct event_header)) {
        struct event_header *header = (struct event_header *) pBuffer;
        pBuffer += sizeof(struct event_header);
        nBufferSize -= sizeof(struct event_header);

        uint8_t event_id = header->id;
        uint64_t timestamp = header->timestamp;
        
        switch (event_id) {
        case ET_EVENT_CURR_VOLT_ENERGY: {
            if (nBufferSize < sizeof(struct event_data_8)) {
                fprintf(stderr, "Buffer too small for event data\n");
                return;
            }

            struct event_data_8 *data = (struct event_data_8 *) pBuffer;
            pBuffer += sizeof(struct event_data_8);
            nBufferSize -= sizeof(struct event_data_8);

            printf("%"PRIu64" %"PRIu32" %"PRIu16" %"PRIu32"\n", timestamp, data->current, data->voltage, data->energy);

            sample_counter++;
            break;
        }
        default:
            fprintf(stderr, "Unknown EnergyTrace event ID: %u\n", event_id);
            // Since we don't know the data length of this event we can't proceed to parse the rest of the buffer
            return;
        }
    }
}

static void et_callback_error(void *pContext, const char *pszErrorText) {
    (void) pContext;

    fprintf(stderr, "EnergyTrace error: %s\n", pszErrorText);
}

static void print_msp430_error(const char *msg) {
    int32_t err = MSP430_Error_Number();
    const char *err_str = MSP430_Error_String(err);
    fprintf(stderr, "%s: %s\n", msg, err_str);
}

static void print_help(const char *program_name) {
    fprintf(stderr, "Usage: %s [duration]\n", program_name);
    fprintf(stderr, "\n");
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "  duration: Measurement duration in seconds. Default: %u\n", DEFAULT_DURATION);
}

int parse_int(const char *str, int base, long *value_out) {
    char *end;
    long value = strtol(str, &end, base);
    if (*end != '\0') {
        return -1;
    }
    *value_out = value;
    return 0;
}

int main(int argc, char *argv[]) {
    int ret = 0;

    int32_t vcc_mv = DEFAULT_VCC;
    unsigned int duration = DEFAULT_DURATION;

    if (argc == 1) {
        // no options given
    } else if (argc == 2) {
        long duration_parsed;
        int res = parse_int(argv[1], 10, &duration_parsed);
        if (res != 0) {
            fprintf(stderr, "Invalid value for duration.\n");
            print_help(argv[0]);
            return 1;
        }
        duration = duration_parsed;
    } else {
        fprintf(stderr, "Invalid invocation.\n");
        print_help(argv[0]);
        return 1;
    }

    fprintf(stderr, "Option values:\n");
    fprintf(stderr, "  duration = %u s\n", duration);
    fprintf(stderr, "\n");

    STATUS_T status;

    char port[] = "TIUSB";
    int32_t version;
    status = MSP430_Initialize(port, &version);
    if (status != STATUS_OK) {
        print_msp430_error("Failed to initialize MSP430 interface");
        return 1;
    }
    fprintf(stderr, "MSP430 interface initialized (DLL version %i)\n", version);

    fprintf(stderr, "Setting VCC to %i mV\n", vcc_mv);
    status = MSP430_VCC(vcc_mv);
    if (status != STATUS_OK) {
        print_msp430_error("Failed to set VCC");
        ret = 1;
        goto close_interface;
    }

    int32_t cur_vcc;
    status = MSP430_GetCurVCCT(&cur_vcc);
    if (status == STATUS_OK) {
        fprintf(stderr, "Current VCC is %i mV\n", cur_vcc);
    } else {
        print_msp430_error("Unable to read current VCC");
    }

    EnergyTraceSetup etSetup = {
        ET_PROFILING_ANALOG,
        ET_PROFILING_10K,
        ET_ALL,
        ET_EVENT_WINDOW_100,
        ET_CALLBACKS_CONTINUOUS
    };
    EnergyTraceCallbacks etCallbacks = {
        .pContext = NULL,
        .pPushDataFn = et_callback_data,
        .pErrorOccurredFn = et_callback_error
    };

    EnergyTraceHandle et;

    cb_counter = 0;
    sample_counter = 0;

    status = MSP430_EnableEnergyTrace(&etSetup, &etCallbacks, &et);
    if (status != STATUS_OK) {
        print_msp430_error("Failed to enable EnergyTrace");
        ret = 1;
        goto close_interface;
    }

    // status = MSP430_ResetEnergyTrace(et);
    // if (status != STATUS_OK) {
    //     print_msp430_error("Failed to reset EnergyTrace");
    //     ret = 1;
    //     goto disable_et;
    // }

    fprintf(stderr, "EnergyTrace started\n");
    fprintf(stderr, "Collecting samples for %u s...\n", duration);

    sleep(duration);

// disable_et:
    status = MSP430_DisableEnergyTrace(et);
    if (status != STATUS_OK) {
        print_msp430_error("Failed to disable EnergyTrace");
        ret = 1;
    }

    fprintf(stderr, "EnergyTrace stopped (received %u samples, %u callbacks)\n", sample_counter, cb_counter);

close_interface:
    status = MSP430_Close(0);
    if (status != STATUS_OK) {
        print_msp430_error("Failed to close MSP430 interface");
        ret = 1;
    }

    return ret;
}
