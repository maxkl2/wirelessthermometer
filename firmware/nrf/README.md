# Important notes

- Using an ST-Link reflashed as a J-Link doesn't seem to work perfectly:
  - nrfjprog doesn't work at all ("Could not connect to CPU core. This may indicate that AP protection is enabled.")
    - `nrfjprog --recover` doesn't work either and does not fix the problem
  - openocd only works if the /RESET line is not connected
    - Invocation: `openocd -f interface/jlink.cfg -c 'transport select swd;' -f target/nrf52.cfg`

