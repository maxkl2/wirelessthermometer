/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT ti_hdc302x

#include <zephyr/device.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/sys/util.h>
#include <zephyr/sys/__assert.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/crc.h>

#include "ti_hdc302x.h"

LOG_MODULE_REGISTER(TI_HDC302X, CONFIG_SENSOR_LOG_LEVEL);

static uint8_t ti_hdc302x_crc(const uint8_t *src, size_t len)
{
	return crc8(src, len, TI_HDC302X_CRC_POLY, TI_HDC302X_CRC_INIT, TI_HDC302X_CRC_REFLECT);
}

static int ti_hdc302x_sample_fetch(const struct device *dev, enum sensor_channel chan)
{
	struct ti_hdc302x_data *drv_data = dev->data;
	const struct ti_hdc302x_config *cfg = dev->config;
	uint8_t buf[6];

	__ASSERT_NO_MSG(chan == SENSOR_CHAN_ALL);

	uint16_t cmd;
	int32_t conv_time;

	switch (cfg->lpm) {
		case 0:
			cmd = TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM0;
			conv_time = TI_HDC302X_CONVERSION_TIME_LPM0;
			break;
		case 1:
			cmd = TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM1;
			conv_time = TI_HDC302X_CONVERSION_TIME_LPM1;
			break;
		case 2:
			cmd = TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM2;
			conv_time = TI_HDC302X_CONVERSION_TIME_LPM2;
			break;
		case 3:
			cmd = TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM3;
			conv_time = TI_HDC302X_CONVERSION_TIME_LPM3;
			break;
		default:
			return -ENOTSUP;
	}

	buf[0] = cmd >> 8; // MSB
	buf[1] = cmd & 0xff; // LSB
	if (i2c_write_dt(&cfg->i2c, buf, 2) < 0) {
		LOG_DBG("Failed to write address pointer");
		return -EIO;
	}

	// Wait for the conversion to finish
	k_msleep(conv_time);

	// TODO: handle data not ready (NACK)

	if (i2c_read_dt(&cfg->i2c, buf, 6) < 0) {
		LOG_DBG("Failed to read sample data");
		return -EIO;
	}

	// Also works for some reason? Might be more energy efficient
	// if (i2c_write_read_dt(&cfg->i2c, buf, 2, buf, 6) < 0) {
	// 	LOG_DBG("Failed to read sample data");
	// 	return -EIO;
	// }

	uint8_t temp_crc = ti_hdc302x_crc(buf, 2);
	uint8_t rh_crc = ti_hdc302x_crc(&buf[3], 2);
	if (temp_crc != buf[2] || rh_crc != buf[5]) {
		LOG_DBG("CRC error in sensor response.");
		return -EIO;
	}

	drv_data->t_sample = (buf[0] << 8) | buf[1];
	drv_data->rh_sample = (buf[3] << 8) | buf[4];

	return 0;
}


static int ti_hdc302x_channel_get(const struct device *dev, enum sensor_channel chan, struct sensor_value *val)
{
	struct ti_hdc302x_data *drv_data = dev->data;
	uint64_t tmp;

	/*
	 * See datasheet "Temperature Register" and "Humidity
	 * Register" sections for more details on processing
	 * sample data.
	 */
	if (chan == SENSOR_CHAN_AMBIENT_TEMP) {
		/* val = -45 + 175 * sample / 2^16 */
		tmp = (uint64_t)drv_data->t_sample * 175U;
		val->val1 = (int32_t)(tmp >> 16) - 45;
		val->val2 = ((tmp & 0xFFFF) * 1000000U) >> 16;
	} else if (chan == SENSOR_CHAN_HUMIDITY) {
		/* val = 100 * sample / 2^16 */
		tmp = (uint64_t)drv_data->rh_sample * 100U;
		val->val1 = tmp >> 16;
		/* x * 1000000 / 65536 == x * 15625 / 1024 */
		val->val2 = ((tmp & 0xFFFF) * 15625U) >> 10;
	} else {
		return -ENOTSUP;
	}

	return 0;
}

static const struct sensor_driver_api ti_hdc302x_driver_api = {
	.sample_fetch = ti_hdc302x_sample_fetch,
	.channel_get = ti_hdc302x_channel_get,
};

static uint16_t read16(const struct i2c_dt_spec *i2c, uint16_t cmd)
{
	uint8_t buf_cmd[2] = {cmd >> 8, cmd & 0xff};
	uint8_t buf[3];

	if (i2c_write_read_dt(i2c, buf_cmd, 2, buf, 3) < 0) {
		LOG_ERR("Error reading register.");
	}

	uint8_t crc = ti_hdc302x_crc(buf, 2);
	if (crc != buf[2]) {
		LOG_ERR("CRC error reading register.");
	}

	return buf[0] << 8 | buf[1];
}

static int ti_hdc302x_init(const struct device *dev)
{
	const struct ti_hdc302x_config *cfg = dev->config;

	if (!device_is_ready(cfg->i2c.bus)) {
		LOG_ERR("Bus device is not ready");
		return -ENODEV;
	}

	if (read16(&cfg->i2c, TI_HDC302X_CMD_MANUFID) != TI_HDC302X_MANUFID) {
		LOG_ERR("Failed to get correct manufacturer ID");
		return -EINVAL;
	}

	LOG_INF("Initialized device successfully, using LPM%i", cfg->lpm);

	return 0;
}

#define TI_HDC302X_DEFINE(inst)										\
static struct ti_hdc302x_data ti_hdc302x_data_##inst;				\
																	\
static const struct ti_hdc302x_config ti_hdc302x_config_##inst = {	\
	.i2c = I2C_DT_SPEC_INST_GET(inst),								\
	.lpm = DT_INST_PROP(inst, low_power_mode),						\
};																	\
																	\
SENSOR_DEVICE_DT_INST_DEFINE(inst, ti_hdc302x_init, NULL,			\
		      &ti_hdc302x_data_##inst, &ti_hdc302x_config_##inst,	\
			  POST_KERNEL, CONFIG_SENSOR_INIT_PRIORITY,				\
			  &ti_hdc302x_driver_api);								\

DT_INST_FOREACH_STATUS_OKAY(TI_HDC302X_DEFINE)
