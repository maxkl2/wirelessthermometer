/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef ZEPHYR_DRIVERS_SENSOR_TI_HDC302X_TI_HDC302X_H_
#define ZEPHYR_DRIVERS_SENSOR_TI_HDC302X_TI_HDC302X_H_

#include <zephyr/kernel.h>

#define TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM0 0x2400
#define TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM1 0x240B
#define TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM2 0x2416
#define TI_HDC302X_CMD_TRIGGER_ON_DEMAND_LPM3 0x24FF
#define TI_HDC302X_CMD_MANUFID 0x3781

#define TI_HDC302X_MANUFID 0x3000

// Conversion times in ms
#define TI_HDC302X_CONVERSION_TIME_LPM0 13
#define TI_HDC302X_CONVERSION_TIME_LPM1 8
#define TI_HDC302X_CONVERSION_TIME_LPM2 5
#define TI_HDC302X_CONVERSION_TIME_LPM3 4

#define TI_HDC302X_CRC_POLY 0x31
#define TI_HDC302X_CRC_INIT 0xFF
#define TI_HDC302X_CRC_REFLECT false

struct ti_hdc302x_config {
	struct i2c_dt_spec i2c;
	int lpm;
};

struct ti_hdc302x_data {
	uint16_t t_sample;
	uint16_t rh_sample;
};

#endif
