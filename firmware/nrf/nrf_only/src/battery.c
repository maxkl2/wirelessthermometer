
#include "battery.h"

#include <math.h>

#include <zephyr/drivers/sensor.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/sys/util.h>
#include <zephyr/logging/log.h>

LOG_MODULE_DECLARE(app, CONFIG_THERMOHYGRO_LOG_LEVEL);

static const struct adc_dt_spec adc_channel = ADC_DT_SPEC_GET_BY_IDX(DT_PATH(zephyr_user), 0);
static uint16_t adc_buf;
static struct adc_sequence adc_sequence = {
    .buffer = &adc_buf,
    .buffer_size = sizeof(adc_buf),
};

static const struct device *const temp_dev = DEVICE_DT_GET(DT_ALIAS(internal_temp));
static struct sensor_value temp_value;

static int16_t batt_voltage_mv;
static int16_t batt_temp;
static uint8_t batt_percent;

struct batt_map_pair {
	int16_t voltage_mv;
	float percent;
};

// Battery voltage to percentage mapping (20 °C)
static const struct batt_map_pair mapping_20C[] = {
    // {mV, %}
    {2000,   0.0},
    {2500,  10.0},
    {2600,  15.0},
    {2700,  22.0},
    {2800,  32.0},
    {2850,  40.0},
    {2900,  63.0},
    {3000,  90.0},
    {3100,  95.0},
    {3200, 100.0},
};

// Battery voltage to percentage mapping (-10 °C)
static const struct batt_map_pair mapping__10C[] = {
    // {mV, %}
    {2000,   0},
    {2100,   5},
    {2200,  10},
    {2300,  15},
    {2400,  30},
    {2500,  50},
    {2600,  80},
    {2700,  97},
    {2800,  99},
    {3200, 100},
};

// Battery voltage to percentage mapping (60 °C)
static const struct batt_map_pair mapping_60C[] = {
    // {mV, %}
    {2000,   0},
    {2500,   3},
    {2600,   4},
    {2700,  10},
    {2800,  21},
    {2850,  29},
    {2900,  41},
    {3000,  99},
    {3200, 100},
};

static float interp_i16_f(int16_t x, int16_t x0, int16_t x1, float y0, float y1) {
	int16_t dx = x1 - x0;
	int16_t dy = y1 - y0;
	float p = (float) (x - x0) / dx;
	return y0 + p * dy;
}

static float map_voltage_to_percent(int16_t voltage_mv, const struct batt_map_pair mapping[], size_t mapping_len) {
	for (size_t i = 0; i < mapping_len; i++) {
		if (voltage_mv < mapping[i].voltage_mv) {
			if (i == 0) {
				return mapping[0].percent;
			}

			size_t prev_i = i - 1;

			return interp_i16_f(
				voltage_mv,
				mapping[prev_i].voltage_mv, mapping[i].voltage_mv,
				mapping[prev_i].percent, mapping[i].percent
			);
		}
	}
	return mapping[mapping_len - 1].percent;
}

static float map_voltage_to_percent_temp_compensated(int16_t voltage_mv, int16_t temp) {
	if (temp < 20) {
		float percent__10 = map_voltage_to_percent(voltage_mv, mapping__10C, ARRAY_SIZE(mapping__10C));
		float percent_20 = map_voltage_to_percent(voltage_mv, mapping_20C, ARRAY_SIZE(mapping_20C));
		return interp_i16_f(
			temp,
			-10, 20,
			percent__10, percent_20
		);
	} else {
		float percent_20 = map_voltage_to_percent(voltage_mv, mapping_20C, ARRAY_SIZE(mapping_20C));
		float percent_60 = map_voltage_to_percent(voltage_mv, mapping_60C, ARRAY_SIZE(mapping_60C));
		return interp_i16_f(
			temp,
			20, 60,
			percent_20, percent_60
		);
	}
}

static int battery_update_voltage() {
    int err;

    err = adc_read_dt(&adc_channel, &adc_sequence);
    if (err < 0) {
        LOG_ERR("Could not read (%d)", err);
        return err;
    }

    int32_t val_mv = (int32_t) adc_buf;
    err = adc_raw_to_millivolts_dt(&adc_channel, &val_mv);
    if (err < 0) {
        LOG_ERR("Failed to convert to mV (%d)", err);
        return err;
    }
    batt_voltage_mv = val_mv;

    return 0;
}

static int32_t sensor_value_to_int(const struct sensor_value *val) {
	if (val->val2 >= 500000) {
		return val->val1 + 1;
	} else {
		return val->val1;
	}
}

static int battery_update_temp() {
    int err;

    err = sensor_sample_fetch_chan(temp_dev, SENSOR_CHAN_DIE_TEMP);
	if (err < 0) {
		LOG_ERR("Could not fetch temperature: %d", err);
		return err;
	}

	err = sensor_channel_get(temp_dev, SENSOR_CHAN_DIE_TEMP, &temp_value);
	if (err < 0) {
		LOG_ERR("Could not get temperature: %d", err);
	}

    batt_temp = sensor_value_to_int(&temp_value);

    return 0;
}

int battery_init() {
    int err;

	if (!adc_is_ready_dt(&adc_channel)) {
		LOG_ERR("ADC controller device %s not ready", adc_channel.dev->name);
		return -1;
	}

	err = adc_channel_setup_dt(&adc_channel);
	if (err != 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	}

	err = adc_sequence_init_dt(&adc_channel, &adc_sequence);
    if (err != 0) {
		LOG_ERR("Failed to init ADC sequence (%d)", err);
		return err;
	}

    if (!device_is_ready(temp_dev)) {
		LOG_ERR("Device %s is not ready", temp_dev->name);
		return -1;
	}

	LOG_DBG("Temperature device is %p, name is %s", temp_dev, temp_dev->name);

    return 0;
}

int battery_update() {
    int err;

    err = battery_update_voltage();
    if (err < 0) {
        LOG_ERR("Could not update voltage (%d)", err);
        return err;
    }

    err = battery_update_temp();
    if (err < 0) {
        LOG_ERR("Could not update temperature (%d)", err);
        return err;
    }

    float batt_percent_f = map_voltage_to_percent_temp_compensated(batt_voltage_mv, batt_temp);

    batt_percent = (int16_t) roundf(batt_percent_f);

    return 0;
}

int16_t battery_get_voltage() {
    return batt_voltage_mv;
}

int16_t battery_get_temp() {
    return batt_temp;
}

uint8_t battery_get_percent() {
    return batt_percent;
}
