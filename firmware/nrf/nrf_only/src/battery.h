
#pragma once

#include <stdint.h>

/**
 * Initializes battery state measurement peripherals.
 *
 * @return 0 if success, error code if failure.
 */
int battery_init();

/**
 * Updates and stores measurements of battery state.
 *
 * @return 0 if success, error code if failure.
 */
int battery_update();

/**
 * Provides last measured value of battery voltage.
 *
 * @note Call battery_update() to update the value.
 * 
 * @return Battery voltage in mV.
 */
int16_t battery_get_voltage();

/**
 * Provides last measured value of battery temperature.
 *
 * @note Call battery_update() to update the value.
 * 
 * @return Battery temperature in °C.
 */
int16_t battery_get_temp();

/**
 * Provides last calculated battery charge percentage.
 *
 * @note Call battery_update() to update the value.
 * 
 * @return Battery percentage.
 */
uint8_t battery_get_percent();
