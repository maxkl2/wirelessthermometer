
#include "led_control.h"

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>


#define LED_JOIN_BLINK_TIME_OFF_MS 100
#define LED_JOIN_BLINK_TIME_ON_MS 50
#define LED_JOIN_BLINK_COUNT 5

#define LED_IDENTIFY_BLINK_TIME_MS 500

#define LED_MEASURE_BLINK_TIME_MS 50

#define LED_NODE DT_NODELABEL(status_led)


static void start_error(struct k_work *work);
static void stop_error(struct k_work *work);

static void start_join(struct k_work *work);
static void toggle_join(struct k_work *work);

static void start_identify(struct k_work *work);
static void stop_identify(struct k_work *work);
static void toggle_identify(struct k_work *work);

static void start_measure(struct k_work *work);
static void stop_measure(struct k_work *work);


static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED_NODE, gpios);


static K_WORK_DEFINE(work_start_error, start_error);
static K_WORK_DEFINE(work_stop_error, stop_error);

static K_WORK_DEFINE(work_start_join, start_join);
static K_WORK_DELAYABLE_DEFINE(work_toggle_join, toggle_join);

static K_WORK_DEFINE(work_start_identify, start_identify);
static K_WORK_DEFINE(work_stop_identify, stop_identify);
static K_WORK_DELAYABLE_DEFINE(work_toggle_identify, toggle_identify);

static K_WORK_DEFINE(work_start_measure, start_measure);
static K_WORK_DELAYABLE_DEFINE(work_stop_measure, stop_measure);


static bool error_active, join_active, identify_active, measure_active;
static bool led_state_join, led_state_identify;

static uint32_t join_counter;


int led_control_init() {
    int err;

    if (!gpio_is_ready_dt(&led)) {
		return ENODEV;
	}

	err = gpio_pin_configure_dt(&led, GPIO_OUTPUT_INACTIVE);
	if (err != 0) {
		return err;
	}

    return 0;
}

void led_control_error(bool enable) {
    if (enable) {
        k_work_submit(&work_start_error);
    } else {
        k_work_submit(&work_stop_error);
    }
}

void led_control_network_join() {
    k_work_submit(&work_start_join);
}

void led_control_identify(bool enable) {
    if (enable) {
        k_work_submit(&work_start_identify);
    } else {
        k_work_submit(&work_stop_identify);
    }
}

void led_control_measure() {
    k_work_submit(&work_start_measure);
}


static void update_led() {
    bool led_state;
    if (error_active) {
        led_state = true;
    } else if (join_active) {
        led_state = led_state_join;
    } else if (identify_active) {
        led_state = led_state_identify;
    } else if (measure_active) {
        led_state = true;
    } else {
        led_state = false;
    }

    gpio_pin_set_dt(&led, led_state);
}

static void start_error(struct k_work *work) {
    (void) work;

    error_active = true;
    update_led();
}

static void stop_error(struct k_work *work) {
    (void) work;

    error_active = false;
    update_led();
}

static void start_join(struct k_work *work) {
    (void) work;

    // Might already be running -> restart                      
    k_work_cancel_delayable(&work_toggle_join);

    join_active = true;
    led_state_join = true;
    join_counter = 0;

    update_led();

    k_work_schedule(&work_toggle_join, K_MSEC(LED_JOIN_BLINK_TIME_ON_MS));
}

static void toggle_join(struct k_work *work) {
    (void) work;

    led_state_join = !led_state_join;
    if (led_state_join == false) {
        join_counter++;
        if (join_counter == LED_JOIN_BLINK_COUNT) {
            join_active = false;
        }
    }

    update_led();

    if (join_active) {
        k_work_schedule(&work_toggle_join, K_MSEC(led_state_join ? LED_JOIN_BLINK_TIME_ON_MS : LED_JOIN_BLINK_TIME_OFF_MS));
    }
}

static void start_identify(struct k_work *work) {
    (void) work;

    // Might already be running -> restart
    k_work_cancel_delayable(&work_toggle_identify);

    identify_active = true;
    led_state_identify = true;

    update_led();

    k_work_schedule(&work_toggle_identify, K_MSEC(LED_IDENTIFY_BLINK_TIME_MS));
}

static void stop_identify(struct k_work *work) {
    (void) work;

    identify_active = false;

    update_led();

    k_work_cancel_delayable(&work_toggle_identify);
}

static void toggle_identify(struct k_work *work) {
    (void) work;

    led_state_identify = !led_state_identify;

    update_led();

    k_work_schedule(&work_toggle_identify, K_MSEC(LED_IDENTIFY_BLINK_TIME_MS));
}

static void start_measure(struct k_work *work) {
    (void) work;

    // Might already be running -> restart
    k_work_cancel_delayable(&work_stop_measure);

    measure_active = true;

    update_led();

    k_work_schedule(&work_stop_measure, K_MSEC(LED_MEASURE_BLINK_TIME_MS));
}

static void stop_measure(struct k_work *work) {
    (void) work;

    measure_active = false;

    update_led();
}
