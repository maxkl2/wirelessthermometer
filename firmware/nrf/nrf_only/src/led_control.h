
#pragma once

#include <stdbool.h>

/**
 * Initializes LED control.
 *
 * @return 0 if success, error code if failure.
 */
int led_control_init();

/**
 * Signal error by lighting the LED continuously.
 *
 * @param enable Whether to enable or disable the error state.
 */
void led_control_error(bool enable);

/**
 * Signal that the device joined a network by quickly blinking several times.
 */
void led_control_network_join();

/**
 * Identify the device by slowly blinking continuously.
 *
 * @param enable Whether to enable or disable the identification state.
 */
void led_control_identify(bool enable);

/**
 * Signal that a measurement was/will be taken by shortly flashing the LED once.
 */
void led_control_measure();
