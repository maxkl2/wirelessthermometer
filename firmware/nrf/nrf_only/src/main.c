
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>
#include <ram_pwrdn.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "led_control.h"
#include "sensor.h"
#include "battery.h"
#include "zb_control.h"

LOG_MODULE_REGISTER(app, CONFIG_THERMOHYGRO_LOG_LEVEL);

int main(void) {
	int err;

	err = led_control_init();
	if (err != 0) {
		LOG_ERR("Failed to initialize LEDs (%d)", err);
		goto startup_error;
	}

	err = sensor_init();
	if (err != 0) {
		LOG_ERR("Failed to initialize sensors (%d)", err);
		goto startup_error;
	}

	err = battery_init();
	if (err != 0) {
		LOG_ERR("Failed to initialize battery measurement (%d)", err);
		goto startup_error;
	}

	zb_control_init();

	if (IS_ENABLED(CONFIG_RAM_POWER_DOWN_LIBRARY)) {
		power_down_unused_ram();
	}

	zb_control_start();

	return 0;

startup_error:
	led_control_error(true);
	return 0;
}
