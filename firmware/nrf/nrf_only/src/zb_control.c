
#include "zb_control.h"

#include <math.h>
#include <dk_buttons_and_leds.h>
#include <zb_nrf_platform.h>
#include <zboss_api.h>
#include <zboss_api_addons.h>
#include <zigbee/zigbee_app_utils.h>
#include <zigbee/zigbee_error_handler.h>
#include <zcl/zb_zcl_temp_measurement_addons.h>
#include <zcl/zb_zcl_el_measurement.h>
#include <zephyr/logging/log.h>

#include "led_control.h"
#include "sensor.h"
#include "battery.h"
#include "zigbee_missing.h"

// Zigbee Cluster Library 4.4.2.2.1.1: MeasuredValue = 100x temperature in degrees Celsius
#define ZCL_TEMP_MULTIPLIER 100
// Zigbee Cluster Library 4.7.2.1.1: MeasuredValue = 100x water content in %
#define ZCL_RH_MULTIPLIER 100

#define BATT_VOLTAGE_DIVISOR 1000

#define THERMOHYGRO_ENDPOINT_NB 1

/* Version of the application software (1 byte). */
#define THERMOHYGRO_APP_VERSION 1

/* Version of the implementation of the Zigbee stack (1 byte). */
#define THERMOHYGRO_ZB_STACK_VERSION 10	

/* Version of the hardware of the device (1 byte). */
#define THERMOHYGRO_HW_VERSION 1

/* Manufacturer name (32 bytes). */
#define THERMOHYGRO_MANUF_NAME "Max Klein"

/* Model number assigned by manufacturer (32-bytes long string). */
#define THERMOHYGRO_MODEL_ID "ThermoHygro1.0"

/* First 8 bytes specify the date of manufacturer of the device
 * in ISO 8601 format (YYYYMMDD). The rest (8 bytes) are manufacturer specific.
 */
#define THERMOHYGRO_DATE_CODE "20240704"

/* Describes the physical location of the device (16 bytes).
 * May be modified during commissioning process.
 */
#define THERMOHYGRO_LOCATION_DESC ""

/* Describes the type of physical environment.
 * For possible values see section 3.2.2.2.10 of ZCL specification.
 */
#define THERMOHYGRO_PH_ENV ZB_ZCL_BASIC_ENV_UNSPECIFIED

// TODO: rename, obsolete when adherence to reporting interval is implemented
/* Weather check period */
#define WEATHER_CHECK_PERIOD_MSEC (1000 * CONFIG_WEATHER_CHECK_PERIOD_SECONDS)

// TODO: rename
/* Delay for first weather check */
#define WEATHER_CHECK_INITIAL_DELAY_MSEC (1000 * CONFIG_FIRST_WEATHER_CHECK_DELAY_SECONDS)

// TODO: don't rely on dk_buttons_and_leds.h
#define LED_RED DK_LED1
#define LED_GREEN DK_LED2
#define LED_BLUE DK_LED3

// LED indicating that device successfully joined Zigbee network
#define ZIGBEE_NETWORK_STATE_LED LED_BLUE

// Button used for Finding and Binding and factory reset
#define USER_BUTTON DK_BTN1_MSK

LOG_MODULE_DECLARE(app, CONFIG_THERMOHYGRO_LOG_LEVEL);

// Types

struct zb_zcl_power_config_attrs {
	zb_uint8_t voltage;
    zb_uint8_t size;
    zb_uint8_t quantity;
    zb_uint8_t rated_voltage;
    zb_uint8_t alarm_mask;
    zb_uint8_t voltage_min_threshold;

	zb_uint8_t remaining;
	zb_uint8_t threshold1;
	zb_uint8_t threshold2;
	zb_uint8_t threshold3;
	zb_uint8_t min_threshold;
	zb_uint8_t percent_threshold1;
	zb_uint8_t percent_threshold2;
	zb_uint8_t percent_threshold3;
	zb_uint32_t alarm_state;
};

struct zb_zcl_humidity_measurement_attrs_t {
	zb_int16_t measure_value;
	zb_int16_t min_measure_value;
	zb_int16_t max_measure_value;
};

struct zb_zcl_dc_voltage_measurement_attrs {
	zb_uint32_t measurement_type;
	zb_int16_t dc_voltage;
	zb_uint16_t dc_voltage_multiplier;
	zb_uint16_t dc_voltage_divisor;
};

struct zb_zcl_device_temp_config_attrs {
	zb_int16_t current_temperature;
};

struct zb_device_ctx {
	zb_zcl_basic_attrs_ext_t basic_attr;
	zb_zcl_identify_attrs_t identify_attr;
	struct zb_zcl_power_config_attrs power_config_attrs;
	zb_zcl_temp_measurement_attrs_t temp_attrs;
	struct zb_zcl_humidity_measurement_attrs_t humidity_attrs;

#ifdef CONFIG_EXPOSE_BATT_DETAILS
	struct zb_zcl_dc_voltage_measurement_attrs batt_voltage_attrs;
	struct zb_zcl_device_temp_config_attrs batt_temp_attrs;
#endif
};

// Stores all cluster-related attributes
static struct zb_device_ctx dev_ctx;

// Attributes setup

ZB_ZCL_DECLARE_BASIC_ATTRIB_LIST_EXT(basic_attr_list,
	&dev_ctx.basic_attr.zcl_version,
	&dev_ctx.basic_attr.app_version,
	&dev_ctx.basic_attr.stack_version,
	&dev_ctx.basic_attr.hw_version,
	dev_ctx.basic_attr.mf_name,
	dev_ctx.basic_attr.model_id,
	dev_ctx.basic_attr.date_code,
	&dev_ctx.basic_attr.power_source,
	dev_ctx.basic_attr.location_id,
	&dev_ctx.basic_attr.ph_env,
	dev_ctx.basic_attr.sw_ver
);

ZB_ZCL_DECLARE_IDENTIFY_SERVER_ATTRIB_LIST(identify_server_attr_list,
	&dev_ctx.identify_attr.identify_time // identify_time
);

ZB_ZCL_DECLARE_POWER_CONFIG_BATTERY_ATTRIB_LIST_EXT_FIXED(power_config_attr_list,
	EMPTY,
	&dev_ctx.power_config_attrs.voltage,
	&dev_ctx.power_config_attrs.size,
	&dev_ctx.power_config_attrs.quantity,
	&dev_ctx.power_config_attrs.rated_voltage,
	&dev_ctx.power_config_attrs.alarm_mask,
	&dev_ctx.power_config_attrs.voltage_min_threshold,
	&dev_ctx.power_config_attrs.remaining,
	&dev_ctx.power_config_attrs.threshold1,
	&dev_ctx.power_config_attrs.threshold2,
	&dev_ctx.power_config_attrs.threshold3,
	&dev_ctx.power_config_attrs.min_threshold,
	&dev_ctx.power_config_attrs.percent_threshold1,
	&dev_ctx.power_config_attrs.percent_threshold2,
	&dev_ctx.power_config_attrs.percent_threshold3,
	&dev_ctx.power_config_attrs.alarm_state
);

ZB_ZCL_DECLARE_TEMP_MEASUREMENT_ATTRIB_LIST(temperature_measurement_attr_list,
	&dev_ctx.temp_attrs.measure_value, // value
	&dev_ctx.temp_attrs.min_measure_value, // min_value
	&dev_ctx.temp_attrs.max_measure_value, // max_value
	&dev_ctx.temp_attrs.tolerance // tolerance
);

ZB_ZCL_DECLARE_REL_HUMIDITY_MEASUREMENT_ATTRIB_LIST(humidity_measurement_attr_list,
	&dev_ctx.humidity_attrs.measure_value, // value
	&dev_ctx.humidity_attrs.min_measure_value, // min_value
	&dev_ctx.humidity_attrs.max_measure_value // max_value
);

#ifdef CONFIG_EXPOSE_BATT_DETAILS

ZB_ZCL_START_DECLARE_ATTRIB_LIST_CLUSTER_REVISION(batt_voltage_attr_list, ZB_ZCL_ELECTRICAL_MEASUREMENT)
	ZB_ZCL_SET_ATTR_DESC(ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_MEASUREMENT_TYPE_ID,
		&dev_ctx.batt_voltage_attrs.measurement_type)
	ZB_ZCL_SET_ATTR_DESC(ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_ID,
		&dev_ctx.batt_voltage_attrs.dc_voltage)
	ZB_ZCL_SET_ATTR_DESC(ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_MULTIPLIER_ID,
		&dev_ctx.batt_voltage_attrs.dc_voltage_multiplier)
	ZB_ZCL_SET_ATTR_DESC(ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_DIVISOR_ID,
		&dev_ctx.batt_voltage_attrs.dc_voltage_divisor)
ZB_ZCL_FINISH_DECLARE_ATTRIB_LIST;

ZB_ZCL_START_DECLARE_ATTRIB_LIST_CLUSTER_REVISION(batt_temp_attr_list, ZB_ZCL_DEVICE_TEMP_CONFIG)
	ZB_ZCL_SET_ATTR_DESC(ZB_ZCL_ATTR_DEVICE_TEMP_CONFIG_CURRENT_TEMPERATURE_ID,
		&dev_ctx.batt_temp_attrs.current_temperature)
ZB_ZCL_FINISH_DECLARE_ATTRIB_LIST;

#endif

// Clusters setup

zb_zcl_cluster_desc_t thermohygro_cluster_list[] = {
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_BASIC,
		ARRAY_SIZE(basic_attr_list),
		basic_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_IDENTIFY,
		ARRAY_SIZE(identify_server_attr_list),
		identify_server_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
		ARRAY_SIZE(power_config_attr_list),
		power_config_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ARRAY_SIZE(temperature_measurement_attr_list),
		temperature_measurement_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,
		ARRAY_SIZE(humidity_measurement_attr_list),
		humidity_measurement_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),

#ifdef CONFIG_EXPOSE_BATT_DETAILS
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_ELECTRICAL_MEASUREMENT,
		ARRAY_SIZE(batt_voltage_attr_list),
		batt_voltage_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),
	ZB_ZCL_CLUSTER_DESC(
		ZB_ZCL_CLUSTER_ID_DEVICE_TEMP_CONFIG,
		ARRAY_SIZE(batt_temp_attr_list),
		batt_temp_attr_list,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_MANUF_CODE_INVALID
	),
#endif
};

// Endpoint setup

#ifdef CONFIG_EXPOSE_BATT_DETAILS
#define IN_CLUSTERS_COUNT 7
#else
#define IN_CLUSTERS_COUNT 5
#endif

#define OUT_CLUSTERS_COUNT 0

ZB_DECLARE_SIMPLE_DESC_WRAP(IN_CLUSTERS_COUNT, OUT_CLUSTERS_COUNT);

ZB_AF_SIMPLE_DESC_TYPE(IN_CLUSTERS_COUNT, OUT_CLUSTERS_COUNT) simple_desc_thermohygro = {
	.endpoint = THERMOHYGRO_ENDPOINT_NB,
	.app_profile_id = ZB_AF_HA_PROFILE_ID,
	.app_device_id = ZB_HA_TEMPERATURE_SENSOR_DEVICE_ID,
	.app_device_version = 0,
	.reserved = 0,
	.app_input_cluster_count = IN_CLUSTERS_COUNT,
	.app_output_cluster_count = OUT_CLUSTERS_COUNT,
	.app_cluster_list = {
		ZB_ZCL_CLUSTER_ID_BASIC,
		ZB_ZCL_CLUSTER_ID_IDENTIFY,
		ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,

#ifdef CONFIG_EXPOSE_BATT_DETAILS
		ZB_ZCL_CLUSTER_ID_ELECTRICAL_MEASUREMENT,
		ZB_ZCL_CLUSTER_ID_DEVICE_TEMP_CONFIG,
#endif
	}
};

// Battery percentage, battery alarm state, temperature, humidity
#define THERMOHYGRO_REPORT_ATTR_COUNT 8

ZBOSS_DEVICE_DECLARE_REPORTING_CTX(reporting_info_thermohygro,
	THERMOHYGRO_REPORT_ATTR_COUNT
);

ZB_AF_DECLARE_ENDPOINT_DESC(thermohygro_ep,
	THERMOHYGRO_ENDPOINT_NB, // ep_id
	ZB_AF_HA_PROFILE_ID, // profile_id
	0, // reserved_length
	NULL, // reserved_ptr
	ARRAY_SIZE(thermohygro_cluster_list), // cluster_number
	thermohygro_cluster_list, // cluster_list
	(zb_af_simple_desc_1_1_t *) &simple_desc_thermohygro, // simple_desc
	THERMOHYGRO_REPORT_ATTR_COUNT, // rep_count
	reporting_info_thermohygro, // rep_ctx
	0, // lev_ctrl_count
	NULL // lev_ctrl_ctx
);

// Device context

ZBOSS_DECLARE_DEVICE_CTX_1_EP(thermohygro_ctx,
	thermohygro_ep
);

static bool finding_binding_active = false;

static void mandatory_clusters_attr_init() {
	dev_ctx.basic_attr.zcl_version = ZB_ZCL_VERSION;
	dev_ctx.basic_attr.app_version = THERMOHYGRO_APP_VERSION;
	dev_ctx.basic_attr.stack_version = THERMOHYGRO_ZB_STACK_VERSION;
	dev_ctx.basic_attr.hw_version = THERMOHYGRO_HW_VERSION;
	ZB_ZCL_SET_STRING_VAL(
		dev_ctx.basic_attr.mf_name,
		THERMOHYGRO_MANUF_NAME,
		ZB_ZCL_STRING_CONST_SIZE(THERMOHYGRO_MANUF_NAME));

	ZB_ZCL_SET_STRING_VAL(
		dev_ctx.basic_attr.model_id,
		THERMOHYGRO_MODEL_ID,
		ZB_ZCL_STRING_CONST_SIZE(THERMOHYGRO_MODEL_ID));

	ZB_ZCL_SET_STRING_VAL(
		dev_ctx.basic_attr.date_code,
		THERMOHYGRO_DATE_CODE,
		ZB_ZCL_STRING_CONST_SIZE(THERMOHYGRO_DATE_CODE));
	dev_ctx.basic_attr.power_source = ZB_ZCL_BASIC_POWER_SOURCE_BATTERY;
	ZB_ZCL_SET_STRING_VAL(
		dev_ctx.basic_attr.location_id,
		THERMOHYGRO_LOCATION_DESC,
		ZB_ZCL_STRING_CONST_SIZE(THERMOHYGRO_LOCATION_DESC));
	dev_ctx.basic_attr.ph_env = THERMOHYGRO_PH_ENV;

	dev_ctx.identify_attr.identify_time = ZB_ZCL_IDENTIFY_IDENTIFY_TIME_DEFAULT_VALUE;
}

static void measurements_clusters_attr_init() {
	dev_ctx.power_config_attrs.voltage = 30;
	dev_ctx.power_config_attrs.size = ZB_ZCL_POWER_CONFIG_BATTERY_SIZE_CR123A;
	dev_ctx.power_config_attrs.quantity = 1;
	dev_ctx.power_config_attrs.rated_voltage = 30;
	dev_ctx.power_config_attrs.alarm_mask = 0;
	dev_ctx.power_config_attrs.voltage_min_threshold = 20;
	dev_ctx.power_config_attrs.remaining = ZB_ZCL_POWER_CONFIG_BATTERY_REMAINING_UNKNOWN;
	dev_ctx.power_config_attrs.threshold1 = 0;
	dev_ctx.power_config_attrs.threshold2 = 0;
	dev_ctx.power_config_attrs.threshold3 = 0;
	dev_ctx.power_config_attrs.min_threshold = 0;
	dev_ctx.power_config_attrs.percent_threshold1 = 0;
	dev_ctx.power_config_attrs.percent_threshold2 = 0;
	dev_ctx.power_config_attrs.percent_threshold3 = 0;
	dev_ctx.power_config_attrs.alarm_state = 0;

	dev_ctx.temp_attrs.measure_value = ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_UNKNOWN;
	dev_ctx.temp_attrs.min_measure_value = SENSOR_TEMP_CELSIUS_MIN * ZCL_TEMP_MULTIPLIER;
	dev_ctx.temp_attrs.max_measure_value = SENSOR_TEMP_CELSIUS_MAX * ZCL_TEMP_MULTIPLIER;
	dev_ctx.temp_attrs.tolerance = SENSOR_TEMP_CELSIUS_TOLERANCE * ZCL_TEMP_MULTIPLIER;

	dev_ctx.humidity_attrs.measure_value = ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_UNKNOWN;
	dev_ctx.humidity_attrs.min_measure_value = SENSOR_HUMIDITY_PERCENT_MIN * ZCL_RH_MULTIPLIER;
	dev_ctx.humidity_attrs.max_measure_value = SENSOR_HUMIDITY_PERCENT_MAX * ZCL_RH_MULTIPLIER;

#ifdef CONFIG_EXPOSE_BATT_DETAILS
	dev_ctx.batt_voltage_attrs.measurement_type = ZB_ZCL_ELECTRICAL_MEASUREMENT_DC_MEASUREMENT;
	dev_ctx.batt_voltage_attrs.dc_voltage = (zb_int16_t) 0x8000;
	dev_ctx.batt_voltage_attrs.dc_voltage_multiplier = 1;
	dev_ctx.batt_voltage_attrs.dc_voltage_divisor = BATT_VOLTAGE_DIVISOR;

	dev_ctx.batt_temp_attrs.current_temperature = 0;
#endif
}

static void log_reporting_info() {
	for (int i = 0; i < THERMOHYGRO_REPORT_ATTR_COUNT; i++) {
		zb_zcl_reporting_info_t *rep_info = &reporting_info_thermohygro[i];
		LOG_INF(
			"dir=%u, ep=%u, c_id=%u, c_role=%u, attr=%u, flags=%u, min=%u, max=%u, addr=%u",
			rep_info->direction,
			rep_info->ep,
			rep_info->cluster_id,
			rep_info->cluster_role,
			rep_info->attr_id,
			rep_info->flags,
			rep_info->u.send_info.min_interval,
			rep_info->u.send_info.max_interval,
			rep_info->dst.short_addr
		);
	}
}

static int update_temperature_attr() {
	float value = 0.0f;
	int err = sensor_get_temperature(&value);
	if (err) {
		LOG_ERR("Failed to get sensor temperature: %d", err);
		return err;
	}

	int16_t attr_value = (int16_t) roundf(value * ZCL_TEMP_MULTIPLIER);
	LOG_INF("Attribute T:%10d", attr_value);

	zb_zcl_status_t status = zb_zcl_set_attr_val(
		THERMOHYGRO_ENDPOINT_NB,
		ZB_ZCL_CLUSTER_ID_TEMP_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_TEMP_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *) &attr_value,
		ZB_FALSE
	);
	if (status) {
		LOG_ERR("Failed to set ZCL attribute: %d", status);
		return status;
	}

	return 0;
}

static int update_humidity_attr() {
	float value = 0.0f;
	int err = sensor_get_humidity(&value);
	if (err) {
		LOG_ERR("Failed to get sensor humidity: %d", err);
		return err;
	}

	int16_t attr_value = (int16_t) roundf(value * ZCL_RH_MULTIPLIER);
	LOG_INF("Attribute H:%10d", attr_value);

	zb_zcl_status_t status = zb_zcl_set_attr_val(
		THERMOHYGRO_ENDPOINT_NB,
		ZB_ZCL_CLUSTER_ID_REL_HUMIDITY_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_REL_HUMIDITY_MEASUREMENT_VALUE_ID,
		(zb_uint8_t *) &attr_value,
		ZB_FALSE
	);
	if (status) {
		LOG_ERR("Failed to set ZCL attribute: %d", status);
		return status;
	}

	return 0;
}

static int update_battery_remaining_attr() {
	uint8_t value = battery_get_percent();

	zb_uint8_t attr_value = value * 2;
	LOG_INF("Attribute Batt%%:%6d", attr_value);

	zb_zcl_status_t status = zb_zcl_set_attr_val(
		THERMOHYGRO_ENDPOINT_NB,
		ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_POWER_CONFIG_BATTERY_PERCENTAGE_REMAINING_ID,
		(zb_uint8_t *) &attr_value,
		ZB_FALSE
	);
	if (status) {
		LOG_ERR("Failed to set ZCL attribute: %d", status);
		return status;
	}

	int16_t value_voltage_mv = battery_get_voltage();

	zb_uint8_t attr_value_voltage_tenths = DIV_ROUND_CLOSEST(value, 100);
	LOG_INF("Attribute BattV:%6d", attr_value_voltage_tenths);

	status = zb_zcl_set_attr_val(
		THERMOHYGRO_ENDPOINT_NB,
		ZB_ZCL_CLUSTER_ID_POWER_CONFIG,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_POWER_CONFIG_BATTERY_VOLTAGE_ID,
		(zb_uint8_t *) &attr_value_voltage_tenths,
		ZB_FALSE
	);
	if (status) {
		LOG_ERR("Failed to set ZCL attribute: %d", status);
		return status;
	}

#ifdef CONFIG_EXPOSE_BATT_DETAILS
	zb_int16_t attr_value_voltage = value_voltage_mv * BATT_VOLTAGE_DIVISOR / 1000;
	LOG_INF("Attribute DCV:%8d", attr_value_voltage);

	status = zb_zcl_set_attr_val(
		THERMOHYGRO_ENDPOINT_NB,
		ZB_ZCL_CLUSTER_ID_ELECTRICAL_MEASUREMENT,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_ID,
		(zb_uint8_t *) &attr_value_voltage,
		ZB_FALSE
	);
	if (status) {
		LOG_ERR("Failed to set ZCL attribute: %d", status);
		return status;
	}

	int16_t value_temp = battery_get_temp();

	zb_int16_t attr_value_temp = value_temp;
	LOG_INF("Attribute DevT:%7d", attr_value_temp);

	status = zb_zcl_set_attr_val(
		THERMOHYGRO_ENDPOINT_NB,
		ZB_ZCL_CLUSTER_ID_DEVICE_TEMP_CONFIG,
		ZB_ZCL_CLUSTER_SERVER_ROLE,
		ZB_ZCL_ATTR_DEVICE_TEMP_CONFIG_CURRENT_TEMPERATURE_ID,
		(zb_uint8_t *) &attr_value_temp,
		ZB_FALSE
	);
	if (status) {
		LOG_ERR("Failed to set ZCL attribute: %d", status);
		return status;
	}
#endif

	return 0;
}

static void start_finding_binding(zb_uint8_t param) {
	ZVUNUSED(param);

	if (ZB_JOINED()) {
		if (!finding_binding_active) {
			zb_ret_t zb_err_code = zb_bdb_finding_binding_target(THERMOHYGRO_ENDPOINT_NB);
			if (zb_err_code == RET_OK) {
				finding_binding_active = true;

				LOG_INF("Manually started finding and binding");
			} else if (zb_err_code == RET_INVALID_STATE) {
				LOG_WRN("RET_INVALID_STATE - Couldn't start finding and binding");
			} else {
				ZB_ERROR_CHECK(zb_err_code);
			}
		} else {
			zb_bdb_finding_binding_target_cancel();
			
			finding_binding_active = false;

			LOG_INF("Manually stopped finding and binding");
		}
	} else {
		LOG_WRN("Device not in a network - cannot start finding and binding");
	}
}

static void identify_callback(zb_uint8_t param) {
	if (param) {
		led_control_identify(true);
	} else {
		led_control_identify(false);
	}
}

static void button_changed(uint32_t button_state, uint32_t has_changed) {
	if (USER_BUTTON & has_changed) {
		if (USER_BUTTON & button_state) {
			/* Button changed its state to pressed */
		} else {
			/* Button changed its state to released */
			if (was_factory_reset_done()) {
				/* The long press was for Factory Reset */
				LOG_DBG("After Factory Reset - ignore button release");
			} else   {
				/* Button released before Factory Reset */

				/* Start finding and binding */
				zb_ret_t err = ZB_SCHEDULE_APP_CALLBACK(start_finding_binding, 0);

				if (err) {
					LOG_ERR("Failed to schedule app callback: %d", err);
				}

				/* Inform default signal handler about user input at the device */
				user_input_indicate();
			}
		}
	}

	check_factory_reset_button(button_state, has_changed);
}

static void gpio_init(void) {
	int err = dk_buttons_init(button_changed);

	if (err) {
		LOG_ERR("Cannot init buttons (err: %d)", err);
	}

	err = dk_leds_init();
	if (err) {
		LOG_ERR("Cannot init LEDs (err: %d)", err);
	}
}

static void update_measurements(zb_uint8_t param) {
	ZVUNUSED(param);

	led_control_measure();

	int err = sensor_update_measurements();

	if (err) {
		LOG_ERR("Failed to check weather: %d", err);
	} else {
		err = update_temperature_attr();
		if (err) {
			LOG_ERR("Failed to update temperature: %d", err);
		}

		err = update_humidity_attr();
		if (err) {
			LOG_ERR("Failed to update humidity: %d", err);
		}
	}

	err = battery_update();
	if (err) {
		LOG_ERR("Failed to check battery: %d", err);
	} else {
		err = update_battery_remaining_attr();
		if (err) {
			LOG_ERR("Failed to update battery remaining: %d", err);
		}
	}

	// log_reporting_info();

	zb_ret_t zb_err = ZB_SCHEDULE_APP_ALARM(
		update_measurements,
		0,
		ZB_MILLISECONDS_TO_BEACON_INTERVAL(WEATHER_CHECK_PERIOD_MSEC)
	);
	if (zb_err) {
		LOG_ERR("Failed to schedule app alarm: %d", zb_err);
	}
}

void zboss_signal_handler(zb_bufid_t bufid) {
	zb_zdo_app_signal_hdr_t *signal_header = NULL;
	zb_zdo_app_signal_type_t signal = zb_get_app_signal(bufid, &signal_header);
	zb_ret_t status = ZB_GET_APP_SIGNAL_STATUS(bufid);
	zb_ret_t err = RET_OK;

	// Update network status LED but only for debug configuration
	#ifdef CONFIG_LOG
	zigbee_led_status_update(bufid, ZIGBEE_NETWORK_STATE_LED);
	#endif

	switch (signal) {
		case ZB_ZDO_SIGNAL_SKIP_STARTUP:
			// ZBOSS framework has started - schedule first weather check
			err = ZB_SCHEDULE_APP_ALARM(
				update_measurements,
				0,
				ZB_MILLISECONDS_TO_BEACON_INTERVAL(WEATHER_CHECK_INITIAL_DELAY_MSEC)
			);
			if (err) {
				LOG_ERR("Failed to schedule app alarm: %d", err);
			}
			break;
		case ZB_BDB_SIGNAL_FINDING_AND_BINDING_TARGET_FINISHED:
			finding_binding_active = false;
			break;
		case ZB_BDB_SIGNAL_DEVICE_REBOOT:
			// fall-through
		case ZB_BDB_SIGNAL_STEERING:
			if (status == RET_OK) {
				zb_time_t long_poll_interval = 60000;
				LOG_INF("Configuring long poll interval to %u ms", long_poll_interval);
				zb_zdo_pim_set_long_poll_interval(long_poll_interval);
				zb_set_ed_timeout(ED_AGING_TIMEOUT_64MIN);
				zb_set_keepalive_timeout(ZB_MILLISECONDS_TO_BEACON_INTERVAL(4 * long_poll_interval));

				led_control_network_join();
			}
			break;
		default:
			break;
	}

	ZB_ERROR_CHECK(zigbee_default_signal_handler(bufid));

	/*
	 * All callbacks should either reuse or free passed buffers.
	 * If bufid == 0, the buffer is invalid (not passed).
	 */
	if (bufid) {
		zb_buf_free(bufid);
	}
}

void zb_control_init() {
	register_factory_reset_button(USER_BUTTON);
	gpio_init();

	if (dk_get_buttons() & USER_BUTTON) {
		LOG_INF("Button pressed on startup -> erasing persistent storage");
		zigbee_erase_persistent_storage(ZB_TRUE);
	}

	// Register device context (endpoint)
	ZB_AF_REGISTER_DEVICE_CTX(&thermohygro_ctx);

	// Init Basic and Identify attributes
	mandatory_clusters_attr_init();

	// Init measurements-related attributes
	measurements_clusters_attr_init();

	// Register callback to identify notifications
	ZB_AF_SET_IDENTIFY_NOTIFICATION_HANDLER(THERMOHYGRO_ENDPOINT_NB, identify_callback);

	// Enable Sleepy End Device behavior
	zb_set_rx_on_when_idle(ZB_FALSE);
}

void zb_control_start() {
	/* Start Zigbee stack */
	zigbee_enable();
}
