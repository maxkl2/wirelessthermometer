
#pragma once

// #include <zcl/zb_zcl_common.h>
#include <zboss_api.h>

/// Wrapper around ZB_DECLARE_SIMPLE_DESC() so that macros are properly expanded
#define ZB_DECLARE_SIMPLE_DESC_WRAP(in_clusters_count, out_clusters_count) \
	ZB_DECLARE_SIMPLE_DESC(in_clusters_count, out_clusters_count)

// Power Configuration Cluster

#define ZB_ZCL_DECLARE_POWER_CONFIG_BATTERY_ATTRIB_LIST_EXT_FIXED(attr_list, bat_num,       \
    voltage, size, quantity, rated_voltage, alarm_mask, voltage_min_threshold,              \
    remaining, threshold1, threshold2, threshold3, min_threshold, percent_threshold1,       \
    percent_threshold2, percent_threshold3, alarm_state)                                    \
  ZB_ZCL_START_DECLARE_ATTRIB_LIST_CLUSTER_REVISION(attr_list, ZB_ZCL_POWER_CONFIG)         \
  ZB_ZCL_POWER_CONFIG_BATTERY_ATTRIB_LIST_EXT(bat_num,                                      \
    voltage, size, quantity, rated_voltage, alarm_mask, voltage_min_threshold,              \
    remaining, threshold1, threshold2, threshold3, min_threshold, percent_threshold1,       \
    percent_threshold2, percent_threshold3, alarm_state)                                    \
  ZB_ZCL_FINISH_DECLARE_ATTRIB_LIST

// Electrical Measurement Cluster

#define ZB_SET_ATTR_DESCR_WITH_ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_ID(data_ptr)       \
{                                                                                               \
  ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_ID,                                             \
  ZB_ZCL_ATTR_TYPE_S16,                                                                         \
  ZB_ZCL_ATTR_ACCESS_READ_ONLY | ZB_ZCL_ATTR_ACCESS_REPORTING,                                  \
  (ZB_ZCL_NON_MANUFACTURER_SPECIFIC),                                                           \
  (void*) data_ptr                                                                              \
}

#define ZB_SET_ATTR_DESCR_WITH_ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_MULTIPLIER_ID(data_ptr) \
{                                                                                               \
  ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_MULTIPLIER_ID,                                  \
  ZB_ZCL_ATTR_TYPE_U16,                                                                         \
  ZB_ZCL_ATTR_ACCESS_READ_ONLY,                                                                 \
  (ZB_ZCL_NON_MANUFACTURER_SPECIFIC),                                                           \
  (void*) data_ptr                                                                              \
}

#define ZB_SET_ATTR_DESCR_WITH_ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_DIVISOR_ID(data_ptr) \
{                                                                                               \
  ZB_ZCL_ATTR_ELECTRICAL_MEASUREMENT_DC_VOLTAGE_DIVISOR_ID,                                     \
  ZB_ZCL_ATTR_TYPE_U16,                                                                         \
  ZB_ZCL_ATTR_ACCESS_READ_ONLY,                                                                 \
  (ZB_ZCL_NON_MANUFACTURER_SPECIFIC),                                                           \
  (void*) data_ptr                                                                              \
}

// Device Temperature Configuration Cluster

#define ZB_ZCL_DEVICE_TEMP_CONFIG_CLUSTER_REVISION_DEFAULT ((zb_uint16_t)0x0001u)

enum zb_zcl_device_temp_config_attr_e
{
  ZB_ZCL_ATTR_DEVICE_TEMP_CONFIG_CURRENT_TEMPERATURE_ID                          = 0x0000,
};

#define ZB_SET_ATTR_DESCR_WITH_ZB_ZCL_ATTR_DEVICE_TEMP_CONFIG_CURRENT_TEMPERATURE_ID(data_ptr)  \
{                                                                                               \
  ZB_ZCL_ATTR_DEVICE_TEMP_CONFIG_CURRENT_TEMPERATURE_ID,                                        \
  ZB_ZCL_ATTR_TYPE_S16,                                                                         \
  ZB_ZCL_ATTR_ACCESS_READ_ONLY | ZB_ZCL_ATTR_ACCESS_REPORTING,                                  \
  (ZB_ZCL_NON_MANUFACTURER_SPECIFIC),                                                           \
  (void*) data_ptr                                                                              \
}

void zb_zcl_device_temp_config_init_server(void);
void zb_zcl_device_temp_config_init_client(void);

#define ZB_ZCL_CLUSTER_ID_DEVICE_TEMP_CONFIG_SERVER_ROLE_INIT zb_zcl_device_temp_config_init_server
#define ZB_ZCL_CLUSTER_ID_DEVICE_TEMP_CONFIG_CLIENT_ROLE_INIT zb_zcl_device_temp_config_init_client
